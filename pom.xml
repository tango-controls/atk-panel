<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>org.tango-controls.gui</groupId>
    <artifactId>ATKPanel</artifactId>
    <version>6.1-SNAPSHOT</version>

    <name>AtkPanel</name>
    <description>AtkPanel</description>
    <url>https://gitlab.com/tango-controls/atk-panel</url>

    <scm>
        <connection>scm:git:git@gitlab.com:tango-controls/atk-panel.git</connection>
        <developerConnection>scm:git:git@gitlab.com:tango-controls/atk-panel.git</developerConnection>
        <url>https://gitlab.com/tango-controls/atk-panel.git</url>
        <tag>HEAD</tag>
    </scm>

    <licenses>
        <license>
            <name>LGPL-3.0</name>
            <url>https://www.gnu.org/licenses/lgpl-3.0.html</url>
        </license>
    </licenses>

    <developers>
        <developer>
            <id>pons</id>
            <name>Jean Luc Pons</name>
            <email>pons@esrf.fr</email>
            <organization>ESRF</organization>
            <organizationUrl>http://esrf.eu</organizationUrl>
            <roles>
                <role>Maintainer</role>
            </roles>
            <timezone>1</timezone>
        </developer>
        <developer>
            <id>poncet</id>
            <name>Faranguiss Poncet</name>
            <email>poncet@esrf.fr</email>
            <organization>ESRF</organization>
            <organizationUrl>http://esrf.eu</organizationUrl>
            <roles>
                <role>Maintainer</role>
            </roles>
            <timezone>1</timezone>
        </developer>
    </developers>

    <dependencies>
        <dependency>
            <groupId>org.tango-controls</groupId>
            <artifactId>JTango</artifactId>
            <version>[9.6.8, 10.0.0)</version>
            <type>pom</type>
        </dependency>
        <dependency>
            <groupId>org.tango-controls.atk</groupId>
            <artifactId>ATKWidget</artifactId>
            <version>[9.3.22,)</version>
        </dependency>
        <dependency>
            <groupId>org.tango-controls.atk</groupId>
            <artifactId>ATKCore</artifactId>
            <version>[9.3.22,)</version>
        </dependency>
    </dependencies>

    <build>
        <!-- Use the filtering process to include a resource file -->
        <!-- ONLY include atkpanel.properties file and exclude all other files -->
        <resources>
            <!-- include atkpanel.properties -->
            <resource>
                <directory>src/main/resources</directory>
                <filtering>true</filtering>
                <includes>
                    <include>atkpanel.properties</include>
                </includes>
            </resource>

            <!-- exclude everything else from filtering -->
            <resource>
                <directory>src/main/resources</directory>
                <filtering>false</filtering>
                <excludes>
                    <exclude>atkpanel.properties</exclude>
                </excludes>
            </resource>
        </resources>

        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-release-plugin</artifactId>
                <version>2.5.3</version>
                <configuration>
                    <goals>install</goals>
                    <autoVersionSubmodules>true</autoVersionSubmodules>
                    <tagNameFormat>@{project.version}</tagNameFormat>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-deploy-plugin</artifactId>
                <version>2.8.2</version>
                <configuration>
                    <skip>true</skip>
                </configuration>
            </plugin>
            
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-jar-plugin</artifactId>
                <configuration>
                    <archive>
                        <manifest>
                            <addClasspath>true</addClasspath>
                            <mainClass>atkpanel.MainPanel</mainClass>
                        </manifest>
                    </archive>
                </configuration>
            </plugin>
        </plugins>
        
    </build>

    <profiles>
        <profile>
            <id>release</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-source-plugin</artifactId>
                        <version>3.2.1</version>
                        <executions>
                            <execution>
                                <id>attach-sources</id>
                                <goals>
                                    <goal>jar</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-javadoc-plugin</artifactId>
                        <version>3.0.0</version>
                        <executions>
                            <execution>
                                <id>attach-javadocs</id>
                                <goals>
                                    <goal>jar</goal>
                                </goals>
                                <configuration>
                                    <doclint>none,-syntax</doclint>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-gpg-plugin</artifactId>
                        <version>3.0.1</version>
                        <executions>
                            <execution>
                                <id>sign-artifacts</id>
                                <phase>verify</phase>
                                <goals>
                                    <goal>sign</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                    <plugin>
                        <groupId>org.codehaus.mojo</groupId>
                        <artifactId>versions-maven-plugin</artifactId>
                        <version>2.10.0</version>
                    </plugin>
                    <plugin>
                        <groupId>org.sonatype.plugins</groupId>
                        <artifactId>nexus-staging-maven-plugin</artifactId>
                        <version>1.6.12</version>
                        <extensions>true</extensions>
                        <configuration>
                            <serverId>ossrh</serverId>
                            <nexusUrl>https://oss.sonatype.org/</nexusUrl>
                            <autoReleaseAfterClose>true</autoReleaseAfterClose>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>
        <profile>
            <id>test-reflection-configuration</id>
            <activation>
                <jdk>(1.8,)</jdk>
            </activation>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-surefire-plugin</artifactId>
                        <version>3.0.0-M5</version>
                        <configuration>
                            <argLine>--add-opens java.base/java.lang=ALL-UNNAMED</argLine>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>

    <properties>
        <maven.compiler.source>17</maven.compiler.source>
        <maven.compiler.target>17</maven.compiler.target>
    </properties>

    <distributionManagement>
        <snapshotRepository>
            <id>ossrh</id>
            <url>https://oss.sonatype.org/content/repositories/snapshots</url>
        </snapshotRepository>
        <repository>
            <id>ossrh</id>
            <url>https://oss.sonatype.org/service/local/staging/deploy/maven2/</url>
        </repository>
    </distributionManagement>
</project>