[![logo](http://www.tango-controls.org/static/tango/img/logo_tangocontrols.png)](http://www.tango-controls.org)

## ATKPanel Releases / Java versions


### ATKPanel latest release compatible with Java 17 or higher

Please note that ATKPanel is now compiled using jdk-17 starting from the release 6.0 and onwards.
So you absolutely need a JVM version 17 or higher to execute the ATKPanel latest releases in Maven Central repository.


[![Download](https://img.shields.io/badge/ATKPanel-v6.0-brightgreen) ](https://repo1.maven.org/maven2/org/tango-controls/gui/ATKPanel/6.0/)



### ATKPanel old release compatible with Java 8 or higher

This release is provided for those who would like to use, at run time, an older version of Java (lower than 17).


[![Download](https://img.shields.io/badge/ATKPanel-v5.11-orange) ](https://repo1.maven.org/maven2/org/tango-controls/gui/ATKPanel/5.11/)


